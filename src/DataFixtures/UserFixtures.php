<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $users = array(
            array(
                'email' => 'admin@admin.com',
                'password' => 'admin',
                'role' => 'ROLE_ADMIN',
                'firstName' => 'admin',
                'lastName' => 'admin'
            ),
            array(
                'email' => 'client@one.com',
                'password' => 'client1',
                'role' => 'ROLE_USER',
                'firstName' => 'client',
                'lastName' => 'one'
            ),
            array(
                'email' => 'client@two.com',
                'password' => 'client2',
                'role' => 'ROLE_USER',
                'firstName' => 'client',
                'lastName' => 'one'
            ),
        );

        $companies = array(
            array(
                'name' => 'noname',
                'siren' => '0000000000',
                'activityArea' => 'food',
                'adress' => '10 test st',
                'cp' => '0000000',
                'city' => 'noname',
                'country' => 'fr'
            ),
            array(
                'name' => 'Danone',
                'siren' => '0000000000',
                'activityArea' => 'food',
                'adress' => '10 test st',
                'cp' => '0000000',
                'city' => 'noname',
                'country' => 'fr'
            ),
            array(
                'name' => 'Mondelez',
                'siren' => '0000000000',
                'activityArea' => 'food',
                'adress' => '10 test st',
                'cp' => '0000000',
                'city' => 'noname',
                'country' => 'fr'
            )
        );

        foreach ($users as $key => $data) {
            $company = new Company();

            $company->setName($companies[$key]['name']);
            $company->setSiren($companies[$key]['siren']);
            $company->setActivityArea($companies[$key]['activityArea']);
            $company->setAdress($companies[$key]['adress']);
            $company->setCp($companies[$key]['cp']);
            $company->setCity($companies[$key]['city']);
            $company->setCountry($companies[$key]['country']);
            $manager->persist($company);

            $user = new User();
            $plaintextPassword = $data['password'];
            $user->setEmail($data['email']);
            $user->setRoles([$data['role']]);
            $user->setFirstName($data['firstName']);
            $user->setLastName($data['lastName']);
            $user->setCompany($company);

            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );
            $user->setPassword($hashedPassword);
            $manager->persist($user);
            $manager->persist($company);
        }
        $manager->flush();
    }
}
