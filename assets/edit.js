$(document).ready(function () {
    $.ajax({
        url: 'http://0.0.0.0:8080/api/companies/' + $('#div_render').data('id'),
        type: 'GET',
        headers: {
            Accept: 'application/json',
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer " + localStorage.getItem('token'));
        },
        success: function(response){
            $.each(response,function(key, val){
                if ($('input#company_'+key)) {
                    $('input#company_'+key).attr('name',key);
                    $('input#company_'+key).attr('value',val);
                }
            });
        },
        error: function(response){
            console.log(response);
        }
    });

    $('#btn_submit').on('click',function(){
        var values = {};

        $.each($('form[name=company]').serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });

        $.ajax({
            url: 'http://0.0.0.0:8080/api/companies/' + $('#div_render').data('id'),
            type: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/merge-patch+json',
            },
            data: JSON.stringify(values),
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer " + localStorage.getItem('token'));
            },
            success: function(response){
                console.log(response);
            },
            error: function(response){
                console.log(response);
            }
        });
    });


});