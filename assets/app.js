/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

$('#form_login').on('submit',function(){
    var values = {};
    $.each($(this).serializeArray(), function(i, field) {
        values[field.name] = field.value;
    });

    $.ajax({
        url: 'http://0.0.0.0:8080/api/login_check',
        type: 'POST',
        contentType: "application/json",
        data: JSON.stringify({'username': values['_username'], 'password': values['_password']}),
        success: function(response){
            localStorage.setItem('token',response.token);
        }
    });
});
