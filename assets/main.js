$(document).ready(function () {
    $.ajax({
        url: 'http://0.0.0.0:8080/api/companies/' + $('#div_render').data('id'),
        type: 'GET',
        headers: {
            Accept: 'application/json',
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer " + localStorage.getItem('token'));
        },
        success: function(response){
            $.each(response,function(key, val){
                if ($('li#'+key)) {
                    $('li#'+key).html(key + ' : ' + val);
                }
            });
        },
        error: function(response){
            console.log(response);
        }
    });


});