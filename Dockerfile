# Dockerfile
FROM php:8.1-cli

RUN apt-get update && apt-get install -y \
      wget \
      libmcrypt-dev \
      libpq-dev \
      nodejs \
      npm

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo pdo_pgsql pgsql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions zip exif pcntl gd

# Symfony tool
RUN wget https://get.symfony.com/cli/installer -O - | bash && \
  mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /app
COPY . /app

RUN composer install
RUN npm install
RUN npx webpack build
RUN symfony console lexik:jwt:generate-keypair

EXPOSE 80
CMD symfony serve --allow-http --no-tls --port=80